/*
  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
            соответсвии с правилавми ниже:

  1. Написать кастомные события которые будут менять статус светофора:
  - start: включает зеленый свет
  - stop: включает красный свет
  - night: включает желтый свет, который моргает с интервалом в 1с.
  И зарегистрировать каждое через addEventListener на каждом из светофоров.

  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
      чтобы включить режим "нерегулируемого перекрестка" (моргающий желтый).

  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)
      или зеленый (на второй клик) цвет соотвественно.
      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

  4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
      статуса, на мигающий желтые.
      Двойной, тройной и более клики на кнопку не должны вызывать повторную
      инициализацию инвервала.

*/
const CustomEvents = function() {

  const BUTTON = document.getElementById('Do');
  const TRAFFICLIGHT = document.querySelectorAll('.trafficLight');
  const ARRAY = Array.from(TRAFFICLIGHT);

  let startEv = new CustomEvent("start");
  let stopEv = new CustomEvent("stop");
  let startNightEv = new CustomEvent("startNight");

TRAFFICLIGHT.forEach( trafficLight => {
  trafficLight.addEventListener('start', (e) => {
    if(e.target.classList.contains('green') ){
      e.target.classList.remove('yellow');
      e.target.classList.remove('red');
      e.target.classList.add('green');
    }
  })
  trafficLight.addEventListener('click', (e) => {
    trafficLight.dispatchEvent(startEv);
  })
  trafficLight.addEventListener('stop', (e) => {
    if(e.target.classList.contains('red') ){
      e.target.classList.remove('green');
      e.target.classList.add('red');
    }
  })
  trafficLight.addEventListener('click', (e) => {
    trafficLight.dispatchEvent(stopEv);
  })
  trafficLight.addEventListener('startNight', (e)=>{
      e.target.classList.remove('red');
      e.target.classList.remove('green');
      e.target.classList.add('yellow');
  });
  BUTTON.addEventListener('click', (e) => {
    trafficLight.dispatchEvent(startNightEv);

  })
})

}//end
export default CustomEvents;
